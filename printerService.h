#ifndef PRINTER_H
#define PRINTER_H

#include "db.h"

#include <QObject>


class printerService : public QObject
{
    Q_OBJECT

public:
    explicit printerService(QObject *parent = nullptr, db* database = nullptr, int* activeProject = nullptr);
    bool print(QString fileName, QString htmlToPrint, QString stylesheet);


private:
    db* database;
    int* activeProject;

    QString replacePlaceholders(QString html);
    QString generateHTMLTable(QList<QStringList> table);
    QString generateHTMLTableTestedUserNames(QList<QStringList> table);
    QString generateDetectedVulnerabilitiesTableHTML(QList<QStringList> list);
    QString generateMHCdetectedVulnerabilitiesTableHTML(QList<QStringList> list);
    QString generateAdressesAndPortsUnsortedList();
    QString generateUnsortedList(QString in);
    QString generateUnsortedList(QList<QStringList> in);
    QString generateUnsortedList(QStringList in);
    QString generateMissingHeadersUnsortedList();
};


#endif // PRINTER_H
