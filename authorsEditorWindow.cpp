#include "authorsEditorWindow.h"
#include "ui_authorsEditorWindow.h"

#include <QInputDialog>
#include <qdebug.h>
#include <QMessageBox>

authorsEditorWindow::authorsEditorWindow(QWidget *parent, db *database) :
    QDialog(parent),
    ui(new Ui::authorsEditorWindow)
{
    ui->setupUi(this);
    this->database = database;
    refresh();
}

authorsEditorWindow::~authorsEditorWindow()
{
    delete authors;
    delete ui;
}

void authorsEditorWindow::on_pb_authorAdd_clicked()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                         tr("User name:"), QLineEdit::Normal,
                                         "", &ok);
    if (ok && !name.isEmpty()){
        database->addAuthor(name);
        refresh();
    }
}

void authorsEditorWindow::refresh()
{
    authors = database->getTableModel("Authors");
    ui->tv_authors->setModel(authors);
    ui->tv_authors->hideColumn(0);
}

void authorsEditorWindow::on_pb_authorRemove_clicked()
{
    QItemSelectionModel *select = ui->tv_authors->selectionModel();
    if(!select->hasSelection()){
        return;
    }

    QMessageBox::StandardButton confirm;
    confirm = QMessageBox::question(this, "Remove project", "Are you sure?",
                                   QMessageBox::Yes|QMessageBox::No);

    if(confirm == QMessageBox::Yes){
        QModelIndexList selectedItems = select->selectedRows();
        database->removeAuthor(selectedItems.at(0).data().toString());
        refresh();
    }


}
