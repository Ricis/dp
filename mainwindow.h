#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "owaspTop10EditorWindow.h"
#include "printerService.h"
#include "authorsEditorWindow.h"
#include "importService.h"
#include "exportWindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    db* database = new  db;
    const QString projectsFolder = "/Projects";
    const QString imageFolder = "/Images";
    const QString attachmentsFolder = "/Attachments";
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    importService* import = new importService(this, database, &activeProject);
    owaspTop10EditorWindow* dbEditor = new owaspTop10EditorWindow(this, database);
    printerService* printer = new printerService(this, database, &activeProject);
    authorsEditorWindow* authorsEditor = new authorsEditorWindow(this, database);
    int activeProject = 0;

    void addPicture(QString pictureType);
    void addPictureFromClipboard(QString pictureType, QString pictureName);
    QPixmap getPicture(QString pictureType);
    void refreshGUI();
    void refreshAuthors();
    void refreshOwasp();
    void refreshIdentifiedAddressPort(int row);
    QString escapeString(QString in);
    void importXml();
    void openActiveProjectFolder();


private slots:
    void changeActiveProject(int project);
    void removeProjectFolder(QString projectID);

    void on_action_editOwaspTop10_triggered();
    void on_actionEdit_authors_triggered();
    void on_actionOpen_project_folder_triggered();
    void on_actionChange_project_triggered();

    void on_le_reportName_textEdited(const QString &arg1);
    void on_de_selectedDate_dateChanged(const QDate &date);
    void on_le_reportInfo_textEdited(const QString &arg1);
    void on_lw_authorsAll_itemDoubleClicked(QListWidgetItem *item);
    void on_lw_authorsSelected_itemDoubleClicked(QListWidgetItem *item);
    void on_lw_OWASPAll_itemDoubleClicked(QListWidgetItem *item);
    void on_lw_OWASPSelected_itemDoubleClicked(QListWidgetItem *item);
    void on_pb_addIdentifiedAddress_clicked();
    void on_pb_removeIdentifiedAddress_clicked();
    void on_pb_addIdentifiedAddressPort_clicked();
    void on_pb_removeIdentifiedAddressPort_clicked();
    void on_tv_identifiedAddresses_clicked(const QModelIndex &index);
    void on_pte_whoIs_textChanged();
    void on_pb_addDetectedVulnerability_clicked();
    void on_pb_removeDetectedVulnerability_clicked();
    void on_te_vulnerabilitiesCustomText_textChanged();
    void on_pb_testedUsernNamesAdd_clicked();
    void on_pb_testedUsernNamesRemove_clicked();
    void on_le_serviceName_textChanged(const QString &arg1);
    void on_le_wordlist_textChanged(const QString &arg1);
    void on_te_passwordAttackCustomText_textChanged();
    void on_pb_burpSuitePictureBrowse_clicked();
    void on_te_sqliCustomText_textChanged();
    void on_pb_zapScreenshotBrowse_clicked();
    void on_te_zapCustomText_textChanged();
    void on_pb_burpSuitePictureClipBoard_clicked();
    void on_pb_zapPictureClipBoard_clicked();
    void on_te_qualysCustomText_textChanged();
    void on_pb_qualysScreenshotBrowse_clicked();
    void on_pb_qualysPictureClipBoard_clicked();
    void on_te_qualysCiphersErrors_textChanged();
    void on_te_qualysConfigurationErrors_textChanged();
    void on_pb_sophosScreenShotBrowse_clicked();
    void on_pb_sophosScreenShotClipBoard_clicked();
    void on_te_sophosCustomTextBefore_textChanged();
    void on_te_sophosCustomTextAfter_textChanged();
    void on_pb_missingHeadersAdd_clicked();
    void on_pb_missingHeadersRemove_clicked();


    void on_te_avCustomTextBefore_textChanged();
    void on_pb_avScreenShotBrowse_clicked();
    void on_pb_avScreenShotClipBoard_clicked();
    void on_pb_summaryBold_clicked();
    void on_pb_summaryItalic_clicked();
    void on_pb_summaryUnsortedList_clicked();
    void on_pte_summary_textChanged();
    void on_pb_browseIdentifiedAddress_clicked();
    void on_actionExport_project_triggered();

    void on_pb_summaryUnderline_clicked();
    void on_te_targetsCustomText_textChanged();
    void on_pb_addAtachment_clicked();
    void on_pb_removeAttachment_clicked();
};

#endif // MAINWINDOW_H
