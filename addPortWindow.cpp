#include "addPortWindow.h"
#include "ui_addPortWindow.h"

addPortWindow::addPortWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addPortWindow)
{
    ui->setupUi(this);
    ui->cb_portType->addItem("tcp");
    ui->cb_portType->addItem("udp");
    ui->cb_portState->addItem("open");
    ui->cb_portState->addItem("closed");
    ui->cb_portState->addItem("filtered");
    ui->cb_portState->addItem("unfiltered");
    ui->cb_portState->addItem("open/filtered");
    ui->cb_portState->addItem("closed/filtered");
}

addPortWindow::~addPortWindow()
{
    delete ui;
}

void addPortWindow::on_pb_add_clicked()
{
    portType = ui->cb_portType->currentText();
    portNum = ui->le_portNum->text();
    portState = ui->cb_portState->currentText();
    this->accept();
}
