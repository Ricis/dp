#ifndef STARTUPWINDOW_H
#define STARTUPWINDOW_H

#include "db.h"

#include <QDialog>

namespace Ui {
class startupWindow;
}

class startupWindow : public QDialog
{
    Q_OBJECT

public:
    explicit startupWindow(QWidget *parent = nullptr, db* database = nullptr);
    ~startupWindow();

private:
    Ui::startupWindow *ui;
    db* database;
    QSqlTableModel* projectsModel;
    QWidget* mainWindow;

signals:
    void projectSelected(int projectId);
    void removeProjectFolder(QString projectId);

private slots:
    void on_pb_newProject_clicked();
    void on_tv_projects_doubleClicked(const QModelIndex &index);
    void on_pb_removeProject_clicked();
};

#endif // STARTUPWINDOW_H
