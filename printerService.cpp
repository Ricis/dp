#include "printerService.h"

#include <QDebug>
#include <QFile>
#include <QDate>


printerService::printerService(QObject *parent, db* database, int* activeProject) : QObject(parent)
{
    this->database = database;
    this->activeProject = activeProject;
}

bool printerService::print(QString fileName, QString htmlToPrint, QString stylesheet)
{
    QString replacedHtml = replacePlaceholders(htmlToPrint);

    QFile file("Projects/" + QString::number(*activeProject) + "/" + fileName);
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        out << "<!DOCTYPE html> <html> <head> <style> ";
        out << stylesheet;
        out << " </style> </head>";
        out << replacedHtml;
    }

    return true;
}

QString printerService::replacePlaceholders(QString html)
{
    QString replacedHtml = html;

    //Replace report name
    replacedHtml.replace("$reportName", database->getProjectName(*activeProject));

    //Replace authors
    QStringList authors = database->getProjectAuthors(*activeProject);
    QString authorsString = "";
    for(int i =0; i < authors.length(); i++)
        authorsString += authors.at(i) + " <br>";
    replacedHtml.replace("$reportAuthors", authorsString);

    //Replace report date
    replacedHtml.replace("$reportDate", database->getProjectDate(*activeProject).toString("dd-MM-yyyy"));

    //Replace report info
    replacedHtml.replace("$reportInfo", database->getProjectInfo(*activeProject));

    //Replace tested vulnerabilities
    QString vulnerabilitiesTableHTML = generateHTMLTable(database->getProjectVulnerabilities(*activeProject));
    replacedHtml.replace("$testedVulnerabilities", vulnerabilitiesTableHTML);

    //Replace identified targets
    QString identifiedTargetsTableHTML = generateHTMLTable(database->getIdentifiedTargets(*activeProject));
    replacedHtml.replace("$identifiedTargets", identifiedTargetsTableHTML);

    QString targetsCustomText = database->getCustomString("targetsCustomText", *activeProject);
    replacedHtml.replace("$targetsCustomText", targetsCustomText);

    QString adressesAndPorts = generateAdressesAndPortsUnsortedList();
    replacedHtml.replace("$AdressesAndPorts", adressesAndPorts);

    QString whoIs = database->getCustomString("whoIs", *activeProject);
    whoIs.replace("\n", " <br>");
    replacedHtml.replace("$whoIs", whoIs);

    //Replace detected vulnerabilities
    QList<QStringList> detectedVulnerabilities = database->getDetectedVulnerabilities(*activeProject);


    QString detectedVulnerabilitiesTableHTML = generateDetectedVulnerabilitiesTableHTML(detectedVulnerabilities);
    QString MHCdetectedVulnerabilitiesTableHTML = generateMHCdetectedVulnerabilitiesTableHTML(detectedVulnerabilities);

    replacedHtml.replace("$identifiedVulnerabilities", detectedVulnerabilitiesTableHTML);
    replacedHtml.replace("$MHCVulnerabilities", MHCdetectedVulnerabilitiesTableHTML);

    QString vulnerabilitiesCustomText = database->getCustomString("vulnerabilitiesCustomText", *activeProject);
    replacedHtml.replace("$vulnerabilitiesCustomText", vulnerabilitiesCustomText);

    QString serviceName = database->getCustomString("passwordAttackServiceName", *activeProject);
    replacedHtml.replace("$passwordAttackServiceName", serviceName);

    QString wordlist = database->getCustomString("wordlist", *activeProject);
    replacedHtml.replace("$wordlist", wordlist);

    QString testedUserNames = generateHTMLTableTestedUserNames(database->getTestedUserNames(*activeProject));
    replacedHtml.replace("$testedUserNames", testedUserNames);

    QString passwordAttacksCustomText = database->getCustomString("passwordAttacksCustomText", *activeProject);
    replacedHtml.replace("$passwordAttacksCustomText", passwordAttacksCustomText);

    QString crackedUsers = generateHTMLTable(database->getCrackedUsers(*activeProject));
    replacedHtml.replace("$crackedUsers", crackedUsers);


    replacedHtml.replace("$BurpSuiteScreenShot", "Images/" + database->getCustomString("BurpSuiteScreenShot", *activeProject));
    replacedHtml.replace("$sqliCustomText", database->getCustomString("sqliCustomText", *activeProject));

    replacedHtml.replace("$zapScreenShot", "Images/" + database->getCustomString("zapScreenShot", *activeProject));
    replacedHtml.replace("$zapCustomText", database->getCustomString("zapCustomText", *activeProject));

    replacedHtml.replace("$qualysCustomText", database->getCustomString("qualysCustomText", *activeProject));
    replacedHtml.replace("$qualysScreenShot", "Images/" + database->getCustomString("qualysScreenShot", *activeProject));

    QString qualysConfigurationErrors = generateUnsortedList(database->getCustomString("qualysConfigurationErrors", *activeProject));
    replacedHtml.replace("$qualysConfigurationErrors", qualysConfigurationErrors);

    QString qualysCiphersErrors = generateUnsortedList(database->getCustomString("qualysCiphersErrors", *activeProject));
    replacedHtml.replace("$qualysCiphersErrors", qualysCiphersErrors);

    replacedHtml.replace("$sophosCustomTextBefore", database->getCustomString("sophosCustomTextBefore", *activeProject));
    replacedHtml.replace("$sophosScreenShot", "Images/" + database->getCustomString("sophosScreenShot", *activeProject));
    replacedHtml.replace("$sophosCustomTextAfter", database->getCustomString("sophosCustomTextAfter", *activeProject));

    replacedHtml.replace("$missingHeaders", generateMissingHeadersUnsortedList());

    replacedHtml.replace("$avCustomTextBefore", database->getCustomString("avCustomTextBefore", *activeProject));
    replacedHtml.replace("$avScreenShot", "Images/" + database->getCustomString("avScreenShot", *activeProject));

    replacedHtml.replace("$summary", database->getCustomString("summary", *activeProject).replace("\n", "<br>"));

    return replacedHtml;
}

QString printerService::generateHTMLTable(QList<QStringList> table)
{
    QString tableHTML = "";
    for(int rowi = 0; rowi < table.length(); rowi++){
        QStringList row = table.at(rowi);
        tableHTML += "<tr>\n";

        for (int columni = 0; columni < row.length(); ++columni) {
            tableHTML += "<td> " + row.at(columni) + "</td>\n";
        }

        tableHTML += "</tr>\n";
    }
    return tableHTML;
}

QString printerService::generateHTMLTableTestedUserNames(QList<QStringList> table)
{
    QString tableHTML = "";

    for(int i = 0; i < table.length(); i = i + 4){
        tableHTML += "<tr>\n";

        if(i < table.length()){
            tableHTML += "<td> " + table.at(i).at(0) + "</td>\n";
        }else {
            tableHTML += "<td> </td>\n";
        }
        if(i+1 < table.length()){
            tableHTML += "<td> " + table.at(i+1).at(0) + "</td>\n";
        }else {
            tableHTML += "<td> </td>\n";
        }
        if(i+2 < table.length()){
            tableHTML += "<td> " + table.at(i+2).at(0) + "</td>\n";
        }else {
            tableHTML += "<td> </td>\n";
        }
        if(i+3 < table.length()){
            tableHTML += "<td> " + table.at(i+3).at(0) + "</td>\n";
        }else {
            tableHTML += "<td> </td>\n";
        }

        tableHTML += "</tr>\n";
    }

    return tableHTML;
}



QString printerService::generateDetectedVulnerabilitiesTableHTML(QList<QStringList> detectedVulnerabilities)
{
    QList<QStringList> detectedVulnerabilitiesTable;

    for (int i = 0; i < detectedVulnerabilities.length(); ++i) {
        QStringList detectedVulnerabilitiesRow = QStringList()  << QString::number(i+1);
        detectedVulnerabilitiesRow << detectedVulnerabilities.at(i).at(0);
        detectedVulnerabilitiesRow << detectedVulnerabilities.at(i).at(1);
        detectedVulnerabilitiesRow << detectedVulnerabilities.at(i).at(2);
        detectedVulnerabilitiesRow << detectedVulnerabilities.at(i).at(3);
        detectedVulnerabilitiesRow << detectedVulnerabilities.at(i).at(4);
        detectedVulnerabilitiesTable << detectedVulnerabilitiesRow;
    }

    QString detectedVulnerabilitiesTableHTML = generateHTMLTable(detectedVulnerabilitiesTable);

    detectedVulnerabilitiesTableHTML.replace("<td> L</td>", "<td class=\"L\">L.</td>");
    detectedVulnerabilitiesTableHTML.replace("<td> M</td>", "<td class=\"M\">M.</td>");
    detectedVulnerabilitiesTableHTML.replace("<td> H</td>", "<td class=\"H\">H.</td>");
    detectedVulnerabilitiesTableHTML.replace("<td> C</td>", "<td class=\"C\">C.</td>");

    return detectedVulnerabilitiesTableHTML;
}

QString printerService::generateMHCdetectedVulnerabilitiesTableHTML(QList<QStringList> detectedVulnerabilities)
{
    QList<QStringList> MHCdetectedVulnerabilitiesTable;

    for (int i = 0; i < detectedVulnerabilities.length(); ++i) {
        if(detectedVulnerabilities.at(i).at(4) != "L"){
            QStringList MHCdetectedVulnerabilitiesRow = QStringList()  << QString::number(i+1);
            MHCdetectedVulnerabilitiesRow << detectedVulnerabilities.at(i).at(4);
            MHCdetectedVulnerabilitiesRow << detectedVulnerabilities.at(i).at(5);
            MHCdetectedVulnerabilitiesRow << detectedVulnerabilities.at(i).at(6);
            MHCdetectedVulnerabilitiesTable << MHCdetectedVulnerabilitiesRow;
        }
    }

    QString MHCdetectedVulnerabilitiesTableHTML = generateHTMLTable(MHCdetectedVulnerabilitiesTable);
    MHCdetectedVulnerabilitiesTableHTML.replace("<td> M</td>", "<td class=\"M\">M.</td>");
    MHCdetectedVulnerabilitiesTableHTML.replace("<td> H</td>", "<td class=\"H\">H.</td>");
    MHCdetectedVulnerabilitiesTableHTML.replace("<td> C</td>", "<td class=\"C\">C.</td>");

    return MHCdetectedVulnerabilitiesTableHTML;
}

QString printerService::generateAdressesAndPortsUnsortedList()
{
    QString result;
    QStringList identifiedAddresses = database->getIdentifiedTargetsIds(*activeProject);


    for (int addressI = 0; addressI < identifiedAddresses.count(); ++addressI) {
        QString addressID = identifiedAddresses.at(addressI);

        QList<QStringList> ports = database->getIdentifiedTargetsPorts(addressID);
        QStringList open;
        QStringList closed;
        QStringList filtered;
        QStringList unfiltered;
        QStringList openFiltered;
        QStringList closedFiltered;

        for (int portI = 0; portI < ports.count(); ++portI) {
            if(ports.at(portI).at(0) == "open")
                open.append(ports.at(portI).at(1) + " " + ports.at(portI).at(2));
            if(ports.at(portI).at(0) == "closed")
                closed.append(ports.at(portI).at(1) + " " + ports.at(portI).at(2));
            if(ports.at(portI).at(0) == "filtered")
                filtered.append(ports.at(portI).at(1) + " " + ports.at(portI).at(2));
            if(ports.at(portI).at(0) == "unfiltered")
                unfiltered.append(ports.at(portI).at(1) + " " + ports.at(portI).at(2));
            if(ports.at(portI).at(0) == "open/filtered")
                openFiltered.append(ports.at(portI).at(1) + " " + ports.at(portI).at(2));
            if(ports.at(portI).at(0) == "closed/filtered")
                closedFiltered.append(ports.at(portI).at(1) + " " + ports.at(portI).at(2));

        }

        if(open.length() == 0 && closed.count() == 0 && filtered.count() == 0 && unfiltered.count() == 0
                && openFiltered.count() == 0 && closedFiltered.count() == 0)
            continue;

        result += database->getIdenfiedTargetByID(addressID) + " <ul> ";

        if(open.length() != 0){
            result += " <li> Open ";
            result += generateUnsortedList(open);
            result += " </li> ";
        }
        if(closed.length() != 0){
            result += " <li> Closed ";
            result += generateUnsortedList(closed);
            result += " </li> ";
        }
        if(filtered.length() != 0){
            result += " <li> Filtered ";
            result += generateUnsortedList(filtered);
            result += " </li> ";
        }
        if(unfiltered.length() != 0){
            result += " <li> Unfiltered ";
            result += generateUnsortedList(unfiltered);
            result += " </li> ";
        }
        if(openFiltered.length() != 0){
            result += " <li> Open/Filtered ";
            result += generateUnsortedList(openFiltered);
            result += " </li> ";
        }
        if(closedFiltered.length() != 0){
            result += " <li> Closed/Filtered ";
            result += generateUnsortedList(closedFiltered);
            result += " </li> ";
        }

        result += "</ul> ";

    }
    return result;
}

QString printerService::generateUnsortedList(QString in)
{
    QString result;
    QStringList lines = in.split(QRegExp("[\n]"), QString::SkipEmptyParts);
    result += " <ul> ";
    for (int i = 0; i < lines.count(); ++i) {
        result += " <li> " + lines.at(i) + " </li> ";
    }
    result += " </ul> ";
    return result;
}

QString printerService::generateUnsortedList(QList<QStringList> in)
{
    QString result;
    if(in.length() == 0)
        return result;

    result += " <ul> ";
    for (int i = 0; i < in.length(); ++i) {
        if(in.at(i).length() != 0)
            result += " <li> " + generateUnsortedList(in.at(i)) + " </li> \n";
    }
    result += " </ul> \n ";
    return result;
}

QString printerService::generateUnsortedList(QStringList in)
{
    QString result;
    if(in.length() == 0)
        return result;

    result += " <ul> ";
    for (int i = 0; i < in.length(); ++i) {
        result += " <li> " + in.at(i) + " </li> \n";
    }
    result += " </ul> \n ";
    return result;
}

QString printerService::generateMissingHeadersUnsortedList()
{
    QList<QStringList> missingHeadersList = database->getMissingHeaders(*activeProject);
    QString result;

    result += " <ul> ";
    for (int i = 0; i < missingHeadersList.length(); ++i) {
        result += " <li> " + missingHeadersList.at(i).at(0) + " </li> ";
        result += " <ul> <li> " + missingHeadersList.at(i).at(1) + " </ul> </li> \n";
    }
    result += " </ul> ";
    return result;
}
