#ifndef ADDPORTWINDOW_H
#define ADDPORTWINDOW_H

#include <QDialog>

namespace Ui {
class addPortWindow;
}

class addPortWindow : public QDialog
{
    Q_OBJECT

public:
    explicit addPortWindow(QWidget *parent = nullptr);
    QString portType;
    QString portNum;
    QString portState;
    ~addPortWindow();

private slots:
    void on_pb_add_clicked();

private:
    Ui::addPortWindow *ui;
};

#endif // ADDPORTWINDOW_H
