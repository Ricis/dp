#include "startupwindow.h"
#include "ui_startupwindow.h"

#include <QDebug>
#include <qmessagebox.h>

startupWindow::startupWindow(QWidget *parent, db* database) :
    QDialog(parent),
    ui(new Ui::startupWindow)
{
    this->database = database;
    mainWindow = parent;

    ui->setupUi(this);
    projectsModel = database->getProjectsModel();
    ui->tv_projects->setModel(projectsModel);
    ui->tv_projects->hideColumn(0);
    ui->tv_projects->hideColumn(3);
}

startupWindow::~startupWindow()
{
    delete ui;
}

void startupWindow::on_pb_newProject_clicked()
{
    int id = database->addNewProject();
    projectsModel->select();
    emit projectSelected(id);
    this->close();
}

void startupWindow::on_tv_projects_doubleClicked(const QModelIndex &index)
{
    QItemSelectionModel *select = ui->tv_projects->selectionModel();
    if(!select->hasSelection()){
        return;
    }
    QModelIndexList selectedItems = select->selectedRows();

    emit projectSelected(selectedItems.at(0).data().toInt());
    delete select;
    this->close();
}

void startupWindow::on_pb_removeProject_clicked()
{
    QItemSelectionModel *select = ui->tv_projects->selectionModel();
    if(!select->hasSelection()){
        return;
    }
    QModelIndexList selectedItems = select->selectedRows();

    QMessageBox::StandardButton confirm;
    confirm = QMessageBox::question(this, "Remove project", "Are you sure?",
                                   QMessageBox::Yes|QMessageBox::No);

    if(confirm == QMessageBox::Yes){
        QString projectID = selectedItems.at(0).data().toString();
        database->removeProject(projectID);
        projectsModel->select();
        emit removeProjectFolder(projectID);
    }
}
