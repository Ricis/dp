#ifndef IMPORTSERVICE_H
#define IMPORTSERVICE_H

#include "db.h"

#include <QObject>
#include <QtXml>
#include <QFile>

class importService : public QObject
{
    Q_OBJECT
public:
    explicit importService(QObject *parent = nullptr, db* database = nullptr, int* activeProject = nullptr);
    void importXML(QString path);

private:
    db* database;
    int* activeProject;
    void importNmap(QDomElement root);

signals:

public slots:
};

#endif // IMPORTSERVICE_H
