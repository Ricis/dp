#ifndef EXPORTWINDOW_H
#define EXPORTWINDOW_H

#include <QDialog>

namespace Ui {
class exportWindow;
}

class exportWindow : public QDialog
{
    Q_OBJECT

public:
    explicit exportWindow(QWidget *parent = nullptr);
    QString reportCSS;
    QString reportTemplate;
    QString reportName;
    ~exportWindow();

private slots:
    void on_pb_print_clicked();

private:
    Ui::exportWindow *ui;
    const QString templatesFolderName = "/Templates";
    const QString cssFolderName = "/StyleSheets";
    void refreshComboBoxes();
};

#endif // EXPORTWINDOW_H
