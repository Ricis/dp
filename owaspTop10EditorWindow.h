#ifndef DBEDITOR_H
#define DBEDITOR_H

#include <QWidget>
#include <QListWidgetItem>
#include <QDialog>

#include "db.h"

namespace Ui {
class owaspTop10EditorWindow;
}

class owaspTop10EditorWindow : public QDialog
{
    Q_OBJECT

public:
    explicit owaspTop10EditorWindow(QWidget *parent = nullptr, db * database = nullptr);
    ~owaspTop10EditorWindow();

private:
    Ui::owaspTop10EditorWindow *ui;

    db* database;
    QString selectedTableName = "OwaspTop10";
    QSqlTableModel* tableModel = database->getTableModel(selectedTableName);

};

#endif // DBEDITOR_H
