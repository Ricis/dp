#ifndef AUTHORSEDITORWINDOW_H
#define AUTHORSEDITORWINDOW_H

#include <QDialog>

#include "db.h"

namespace Ui {
class authorsEditorWindow;
}

class authorsEditorWindow : public QDialog
{
    Q_OBJECT

public:
    explicit authorsEditorWindow(QWidget *parent = nullptr, db * database = nullptr);
    ~authorsEditorWindow();

private slots:
    void on_pb_authorAdd_clicked();


    void on_pb_authorRemove_clicked();

private:
    db* database;
    Ui::authorsEditorWindow *ui;
    QSqlTableModel* authors;

    void refresh();
};

#endif // AUTHORSEDITORWINDOW_H
