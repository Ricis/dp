#include "db.h"

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QDir>
#include <QList>
#include <QSqlTableModel>
#include <QDate>
#include <QMessageBox>
#include <QSqlRecord>



db::db(QObject *parent) : QObject(parent)
{
    initializeDb();
}

QSqlTableModel* db::getTableModel(QString tableName)
{
    QSqlTableModel *model = new QSqlTableModel();
    model->setTable(tableName);
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    model->select();
    return model;
}

QSqlTableModel* db::getProjectsModel()
{
    QSqlTableModel *model = new QSqlTableModel();
    model->setTable("Projects");
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    model->select();
    model->setHeaderData(1, Qt::Horizontal, "Project name");
    model->setHeaderData(2, Qt::Horizontal, "Date");
    return model;
}


QStringList db::getAllAuthors()
{
    return getStringList("authorName", "Authors");
}
QStringList db::getAllVulnerabilities()
{
    return getStringList("vulnerability", "OwaspTop10");
}

QList<QStringList> db::getVulnerabilitiesTable()
{
    QStringList columnNames = QStringList() << "vulnerabilityNo" << "vulnerability" << "vulnerabilityInfo";
    return getTable(columnNames, "OwaspTop10");
}
QList<QStringList> db::getIdentifiedTargets(int projectID)
{
    QStringList columnNames = QStringList() << "dns" << "ipv4" << "as" << "sp" << "state";
    return getTableFiltered(columnNames, "IdentifiedAddresses", "projectID", QString::number(projectID));
}

QString db::getIdenfiedTargetByID(QString targetID)
{
    return getString("ipv4", "IdentifiedAddresses", "identifiedAddressID", targetID);
}

QString db::getIdenfiedTargetIdByIpAndProjectID(QString ipv4, QString projectID)
{
    return getStringDoubleFiltered("identifiedAddressID", "IdentifiedAddresses", "projectID", projectID, "ipv4", ipv4);
}

QStringList db::getIdentifiedTargetsIds(int projectID)
{
    return getStringListFiltered("identifiedAddressID", "IdentifiedAddresses", "projectID", QString::number(projectID));
}

QList<QStringList> db::getIdentifiedTargetsPorts(QString identifiedAddressID)
{
    QStringList columnNames = QStringList() << "portState" << "portType" << "portNum";
    return getTableFiltered(columnNames, "IdentifiedAddressesPorts", "identifiedAddressID", identifiedAddressID);
}

QList<QStringList> db::getDetectedVulnerabilities(int projectID)
{
    QStringList columnNames = QStringList();
    columnNames << "ip";
    columnNames << "port";
    columnNames << "service";
    columnNames << "vulnerability";
    columnNames << "risk";
    columnNames << "possibleImpact";
    columnNames << "recommendation";
    return getTableFiltered(columnNames, "DetectedVulnerabilities", "projectID", QString::number(projectID));
}

QList<QStringList> db::getProjectVulnerabilities(int projectID)
{
    QList<QStringList> result;
    QSqlQuery query;
    QStringList row;
    query.prepare("SELECT OwaspTop10.vulnerabilityNo, OwaspTop10.vulnerability, OwaspTop10.vulnerabilityInfo FROM OwaspTop10 JOIN ProjectOwasp  ON OwaspTop10.vulnerabilityID = ProjectOwasp.vulnerabilityID JOIN Projects ON Projects.projectID = ProjectOwasp.ProjectID WHERE ProjectOwasp.projectID = " + QString::number(projectID));
    if(!query.exec()){
        error(query.lastError().text());
    }

    int columnCount = query.record().count();
    while(query.next())
    {
        row.clear();
        for(int i = 0; i < columnCount; i++){
            row << query.value(i).toString();
        }
        result << row;
    }
    return result;
}

QList<QStringList> db::getTestedUserNames(int projectID)
{
    QStringList columnNames = QStringList() << "userName";
    return getTableFiltered(columnNames, "TestedUserNames", "projectID", QString::number(projectID));
}

QList<QStringList> db::getCrackedUsers(int projectID)
{
    QList<QStringList> result;
    QStringList columnNames = QStringList() << "userName" << "userPassword";
    QList<QStringList> table = getTableFiltered(columnNames, "TestedUserNames", "projectID", QString::number(projectID));

    for (int i = 0; i < table.length(); ++i) {
        if(table.at(i).at(1) != ""){
            result.append(table.at(i));
        }
    }

    return result;
}

QList<QStringList> db::getMissingHeaders(int projectID)
{
    QStringList columnNames = QStringList() << "missingHeader"  << "detail";
    return getTableFiltered(columnNames, "MissingHeaders", "projectID", QString::number(projectID));
}


QStringList db::getProjectAuthors(int projectID)
{
    QStringList authors;
    QSqlQuery query;
    query.prepare("SELECT Authors.authorName FROM ProjectAuthors JOIN Authors ON Authors.authorID = ProjectAuthors.authorID JOIN Projects ON Projects.projectID = ProjectAuthors.ProjectID WHERE ProjectAuthors.projectID = " + QString::number(projectID));
    if(!query.exec()){
        error(query.lastError().text());
    }

    while(query.next())
    {
        authors.append(query.value(0).toString());
    }

    return authors;
}
QStringList db::getProjectVulnerabilitiesNames(int projectID)
{
    QStringList owasps;
    QSqlQuery query;
    query.prepare("SELECT OwaspTop10.vulnerability FROM OwaspTop10 JOIN ProjectOwasp  ON OwaspTop10.vulnerabilityID = ProjectOwasp.vulnerabilityID JOIN Projects ON Projects.projectID = ProjectOwasp.ProjectID WHERE ProjectOwasp.projectID = " + QString::number(projectID));
    if(!query.exec()){
        error(query.lastError().text());
    }

    while(query.next())
    {
        owasps.append(query.value(0).toString());
    }

    return owasps;
}


QString db::getProjectName(int id)
{
    QString projectName = getString("projectName", "Projects", "projectID", QString::number(id));
    return projectName;
}
QString db::getProjectInfo(int id)
{
    QString projectInfo = getString("projectInfo", "Projects", "projectID", QString::number(id));
    return projectInfo;
}
QDate db::getProjectDate(int id)
{
    QString dateString = getString("projectDate", "Projects", "projectID", QString::number(id));
    QDate date = QDate::fromString(dateString,"dd-MM-yyyy");
    return date;
}

QString db::getCustomString(QString type, int id)
{
   return getString(type, "CustomStrings", "projectID", QString::number(id));
}



void db::setProjectName(int id, QString name)
{
    setString("Projects", "projectName", name, "projectID", QString::number(id));
}
void db::setProjectInfo(int id, QString info)
{
    setString("Projects", "projectInfo", info, "projectID", QString::number(id));
}
void db::setProjectDate(int id, QDate date)
{
    setString("Projects", "projectDate", date.toString("dd-MM-yyyy"), "projectID", QString::number(id));
}


void db::setCustomString(QString type, QString string, int id)
{
    setString("CustomStrings", type, string, "projectID", QString::number(id));
}

int db::addNewProject()
{
    QSqlQuery query;
    query.prepare("INSERT INTO Projects (projectName, projectDate, projectInfo) VALUES ('newProject', '" + QDate::currentDate().toString("dd-MM-yyyy") + "', 'newProjectInfo')");
    if(!query.exec()){
        error(query.lastError().text());
    }
    query.clear();

    query.prepare("SELECT projectID FROM Projects ORDER BY projectID DESC LIMIT 1");
    if(!query.exec()){
        error(query.lastError().text());
    }
    query.first();
    int id = query.value(0).toInt();
    query.clear();

    query.prepare("INSERT INTO CustomStrings (projectID) VALUES ('" + QString::number(id) + "')");
    if(!query.exec()){
        error(query.lastError().text());
    }


    return id;
}

void db::addAuthor(QString name)
{
    QStringList columnNames;
    columnNames << "authorName";

    QStringList params;
    params << name;
    inserNew(columnNames, "Authors", params);
}


void db::addAuthorToProject(QString authorName, int projectId)
{
    QString authorId = getString("authorID", "Authors", "authorName", authorName);
    QSqlQuery query;
    query.prepare("INSERT INTO ProjectAuthors ('AuthorID', 'ProjectID') VALUES (" + authorId + "," + QString::number(projectId) + ")");
    if(!query.exec()){
        error(query.lastError().text());
    }

}
void db::addVulnerabilityToProject(QString vulnerabilityName, int projectId)
{
    QString vulnerabilityID = getString("vulnerabilityID", "OwaspTop10", "vulnerability", vulnerabilityName);
    QSqlQuery query;
    query.prepare("INSERT INTO ProjectOwasp ('vulnerabilityID', 'projectID') VALUES (" + vulnerabilityID + "," + QString::number(projectId) + ")");
    if(!query.exec()){
        error(query.lastError().text());
    }
}

void db::addIdentifiedAddress(QString ipv4, QString dns, QString as, QString sp, QString state, int projectId)
{
//    QList<QStringList> targets = getIdentifiedTargets(projectId);
//    for (int i = 0; i < targets.length(); ++i) {
//        if(targets.at(i).at(1) == ipv4)
//            return;
//    }

    QString queryString = "INSERT INTO IdentifiedAddresses ('projectID', 'ipv4', 'dns', 'as', 'sp', 'state') VALUES (" + QString::number(projectId) + ", '" + ipv4 + "', '" + dns + "', '" + as + "', '" + sp + "', '" + state + "')";
    qWarning() << queryString;
    QSqlQuery query;
    query.prepare(queryString);
    if(!query.exec()){
        error(query.lastError().text());
    }
}

void db::addIdentifiedAddressPort(int identifiedAddressID, QString portType, QString portNum, QString portState)
{

    QList<QStringList> targets = getIdentifiedTargetsPorts(QString::number(identifiedAddressID));
    for (int i = 0; i < targets.length(); ++i) {
        if(targets.at(i).at(2) == portNum)
            return;
    }

    QStringList columnNames;
    columnNames << "identifiedAddressID";
    columnNames << "portType";
    columnNames << "portNum";
    columnNames << "portState";

    QStringList params;
    params << QString::number(identifiedAddressID);
    params << portType;
    params << portNum;
    params << portState;
    inserNew(columnNames, "IdentifiedAddressesPorts", params);
}

void db::addDetectedVulnerability(QStringList params)
{
    QStringList columnNames;
    columnNames << "projectID";
    columnNames << "ip";
    columnNames << "port";
    columnNames << "service";
    columnNames << "vulnerability";
    columnNames << "risk";
    columnNames << "possibleImpact";
    columnNames << "recommendation";
    inserNew(columnNames, "DetectedVulnerabilities", params);
}

void db::addTestedUserName(QString username, QString password, int projectId)
{
    QStringList columnNames;
    columnNames << "projectID";
    columnNames << "userName";
    columnNames << "userPassword";

    QStringList params;
    params << QString::number(projectId);
    params << username;
    params << password;
    inserNew(columnNames, "TestedUserNames", params);
}

void db::addMissingHeader(QString missingHeader, QString detail, int projectId)
{
    QStringList columnNames;
    columnNames << "projectID";
    columnNames << "missingHeader";
    columnNames << "detail";

    QStringList params;
    params << QString::number(projectId);
    params << missingHeader;
    params << detail;
    inserNew(columnNames, "MissingHeaders", params);
}

void db::addAttachment(QString attachment, int projectId)
{
    QStringList columnNames;
    columnNames << "projectID";
    columnNames << "attachmentName";

    QStringList params;
    params << QString::number(projectId);
    params << attachment;
    inserNew(columnNames, "Attachments", params);
}



void db::removeAuthorFromProject(QString authorName, int projectId)
{
    QString authorId = getString("authorID", "Authors", "authorName", authorName);
    QSqlQuery query;
    query.prepare("DELETE FROM ProjectAuthors WHERE authorID=" + authorId + " AND projectID=" + QString::number(projectId));
    if(!query.exec()){
        error(query.lastError().text());
    }
}
void db::removeVulnerabilityFromProject(QString vulnerabilityName, int projectId)
{
    QString vulnerabilityId = getString("vulnerabilityID", "OwaspTOP10", "vulnerability", vulnerabilityName);
    QSqlQuery query;
    query.prepare("DELETE FROM ProjectOwasp WHERE vulnerabilityID=" + vulnerabilityId + " AND projectID=" + QString::number(projectId));
    if(!query.exec()){
        error(query.lastError().text());
    }
}

void db::removeIdentifiedAddress(int id)
{
    removeById("IdentifiedAddresses", "identifiedAddressID", QString::number(id));
}
void db::removeIdentifiedAddress(QString id)
{
    removeById("IdentifiedAddressesPorts", "identifiedAddressID", id);
    removeById("IdentifiedAddresses", "identifiedAddressID", id);
}

void db::removeIdentifiedAddressPort(QString id)
{
    removeById("IdentifiedAddressesPorts", "portID", id);
}

void db::removeDetectedVulnerability(QString id)
{
    removeById("DetectedVulnerabilities", "detectedVulnerabilityID", id);
}

void db::removeTestedUserName(QString id)
{
    removeById("TestedUserNames", "testedUserNameID", id);
}

void db::removeMissingHeader(QString id)
{
    removeById("MissingHeaders", "missingHeaderID", id);
}

void db::removeProject(QString id)
{
    removeById("TestedUserNames", "projectID", id);
    removeById("ProjectOwasp", "projectID", id);
    removeById("ProjectAuthors", "projectID", id);
    removeById("MissingHeaders", "projectID", id);
    QStringList identifiedAddressesIDsToRemove = getStringListFiltered("identifiedAddressID", "IdentifiedAddresses", "projectID", id);
    for (int i = 0; i < identifiedAddressesIDsToRemove.length(); ++i) {
        removeIdentifiedAddress(identifiedAddressesIDsToRemove.at(i));
    }
    removeById("DetectedVulnerabilities", "projectID", id);
    removeById("CustomStrings", "projectID", id);
    removeById("Projects", "projectID", id);
    removeById("Attachments", "projectID", id);
}

void db::removeAuthor(QString authorID)
{
    removeById("ProjectAuthors", "authorID", authorID);
    removeById("Authors", "authorID", authorID);
}

void db::removeAttachment(QString id)
{
    removeById("Attachments", "attachmentID", id);
}


// PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE

QString db::getString(QString whatToGet, QString table, QString identifierName, QString identigierValue)
{
    QString result;
    QSqlQuery query;
    query.prepare("SELECT " + whatToGet + " FROM " + table + " WHERE " + identifierName + "='" + identigierValue + "'" );
    if(!query.exec()){
        error(query.lastError().text());
    }

    if(!query.first())
    {
        qWarning() << "No data returned from: " << query.lastQuery();
        return "";
    }

    result =  query.value(0).toString();
    return result;
}

QString db::getStringDoubleFiltered(QString whatToGet, QString table, QString identifierName1, QString indentifierValue1, QString identifierName2, QString indentifierValue2)
{
    QString result;
    QString queryString = "SELECT " + whatToGet + " FROM " + table;
    queryString += " WHERE " + identifierName1 + "='" + indentifierValue1 + "'";
    queryString += " AND " + identifierName2 + "='" + indentifierValue2 + "'";
    QSqlQuery query;
    query.prepare(queryString);
    if(!query.exec()){
        error(query.lastError().text());
    }

    if(!query.first())
    {
        qWarning() << "getStringDoubleFiltered No data returned from: " << query.lastQuery();
        return "";
    }

    result =  query.value(0).toString();
    return result;
}

QStringList db::getStringList(QString whatToGet, QString table)
{
    QStringList result;
    QSqlQuery query;
    query.prepare("SELECT " + whatToGet + " FROM " + table);
    if(!query.exec()){
        error(query.lastError().text());
    }
    while(query.next())
    {
        result.append(query.value(0).toString());
    }
    return result;
}
QStringList db::getStringListFiltered(QString whatToGet, QString table, QString identifierName, QString indentifierValue)
{
    QStringList result;
    QSqlQuery query;
    query.prepare("SELECT " + whatToGet + " FROM " + table + " WHERE " + identifierName + "='" + indentifierValue + "'" );
    if(!query.exec()){
        error(query.lastError().text());
    }
    while(query.next())
    {
        result.append(query.value(0).toString());
    }
    return result;
}

QList<QStringList> db::getTable(QStringList columnNames, QString tableName)
{
    QList<QStringList> result;
    QString queryString;
    QStringList row;
    QSqlQuery query;

    queryString += "SELECT " ;
    for (int i = 0; i < columnNames.length(); ++i) {
        if(i == columnNames.length()-1)
            queryString += "\"" + columnNames.at(i) + "\" ";
        else
            queryString += "\"" + columnNames.at(i) + "\", ";
    }
    queryString += "FROM " + tableName;


    query.prepare(queryString);
    if(!query.exec()){
        error(query.lastError().text());
    }

    int columnCount = query.record().count();
    while(query.next())
    {
        row.clear();
        for(int i = 0; i < columnCount; i++){
            row << query.value(i).toString();
        }
        result << row;
    }
    return result;
}

QList<QStringList> db::getTableFiltered(QStringList columnNames, QString tableName, QString identifierName, QString identifierValue)
{
    QList<QStringList> result;
    QString queryString;
    QStringList row;
    QSqlQuery query;

    queryString += "SELECT " ;
    for (int i = 0; i < columnNames.length(); ++i) {
        if(i == columnNames.length()-1)
            queryString += "\"" + columnNames.at(i) + "\" ";
        else
            queryString += "\"" + columnNames.at(i) + "\", ";
    }
    queryString += "FROM " + tableName;
    queryString += " WHERE " + identifierName + " = " + identifierValue;

    query.prepare(queryString);
    if(!query.exec()){
        error(query.lastError().text());
    }

    int columnCount = query.record().count();
    while(query.next())
    {
        row.clear();
        for(int i = 0; i < columnCount; i++){
            row << query.value(i).toString();
        }
        result << row;
    }
    return result;
}

void db::inserNew(QStringList columnNames, QString tableName, QStringList parameters)
{
    QString queryString;
    QSqlQuery query;

    queryString += "INSERT INTO " + tableName + " (";
    for (int i = 0; i < columnNames.length(); ++i) {
        if(i == columnNames.length()-1)
            queryString += "'" + columnNames.at(i) + "' ";
        else
            queryString += "'" + columnNames.at(i) + "', ";
    }
    queryString += ") ";

    queryString += "VALUES (";
    for (int i = 0; i < parameters.length(); ++i) {
        if(i == parameters.length()-1)
            queryString += "'" + parameters.at(i) + "' ";
        else
            queryString += "'" + parameters.at(i) + "', ";
    }
    queryString += ") ";

    query.prepare(queryString);
    if(!query.exec()){
        qWarning() << query.lastQuery();
        error(query.lastError().text());
    }
}

void db::sendQuery(QString query)
{
    QSqlQuery q;
    q.prepare(query);
    if(!q.exec()){
        qDebug() << q.lastQuery();
        error(q.lastError().text());
    }
}

void db::setString(QString table, QString whatToSet, QString value, QString identifierName, QString id)
{
    QSqlQuery query;
    query.prepare("UPDATE " + table + " SET " + whatToSet + "='" + value + "' WHERE " + identifierName + "=" + id);
    if(!query.exec()){
        error(query.lastError().text());
    }
}
void db::removeById(QString table, QString identifierName, QString identifierValue)
{
    QSqlQuery query;
    qWarning() << "DELETE FROM " + table + " WHERE " + identifierName + "=" + identifierValue;
    query.prepare("DELETE FROM " + table + " WHERE " + identifierName + "=" + identifierValue);
    if(!query.exec()){
        error(query.lastError().text());
    }
}

void db::initializeDb()
{
    // Check existence of folder and create it
    QDir dir(dbDirectory + "/");
    if (!dir.exists())
        dir.mkpath(".");

    // Create DB Object
    if(QSqlDatabase::isDriverAvailable(DRIVER))
    {
        QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);

        //Check if db file exists
        QString path = dir.absolutePath() +  "/" + dbName;
        if(!QFileInfo::exists(path) || !QFileInfo(path).isFile()){
            db.setDatabaseName(dir.absolutePath() +  "/" + dbName);

            if(!db.open())
                qWarning() << "MainWindow::DatabaseConnect - ERROR: " << db.lastError().text();
            initializeNewDb();

        }else{

            db.setDatabaseName(dir.absolutePath() +  "/" + dbName);

            if(!db.open())
                qWarning() << "MainWindow::DatabaseConnect - ERROR: " << db.lastError().text();
        }


    }
    else
        qWarning() << "MainWindow::DatabaseConnect - ERROR: no driver " << DRIVER << " available";
}

void db::initializeNewDb()
{
    qWarning() << "creating new DB";
    sendQuery("CREATE TABLE \"Attachments\" (\"attachmentID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"projectID\"	INTEGER NOT NULL,\"attachmentName\"	TEXT,FOREIGN KEY(\"projectID\") REFERENCES \"Projects\"(\"projectID\"))");
    sendQuery("CREATE TABLE \"Authors\" (\"authorID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"authorName\"	TEXT)");
    sendQuery("CREATE TABLE \"CustomStrings\" (\"customStringID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"projectID\"	INTEGER NOT NULL,\"vulnerabilitiesCustomText\"	TEXT,\"passwordAttackServiceName\"	TEXT, wordlist TEXT, passwordAttacksCustomText TEXT, BurpSuiteScreenShot TEXT, sqliCustomText TEXT, zapScreenShot TEXT, zapCustomText TEXT, qualysCustomText TEXT, qualysScreenShot TEXT, qualysConfigurationErrors TEXT, qualysCiphersErrors TEXT, sophosCustomTextBefore TEXT, sophosScreenShot TEXT, sophosCustomTextAfter TEXT, whoIs TEXT, avCustomTextBefore TEXT, avScreenShot TEXT, summary TEXT, targetsCustomText TEXT,FOREIGN KEY(\"projectID\") REFERENCES \"Projects\"(\"projectID\"))");
    sendQuery("CREATE TABLE \"DetectedVulnerabilities\" (\"detectedVulnerabilityID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"projectID\"	INTEGER,\"ip\"	TEXT,\"port\"	TEXT,\"service\"	TEXT,\"vulnerability\"	TEXT,\"risk\"	TEXT,\"possibleImpact\"	TEXT,\"recommendation\"	TEXT,FOREIGN KEY(\"projectID\") REFERENCES \"Projects\"(\"projectID\"))");
    sendQuery("CREATE TABLE \"IdentifiedAddresses\" (\"identifiedAddressID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"projectID\"	INTEGER NOT NULL,\"ipv4\"	TEXT,\"dns\"	TEXT,\"as\"	TEXT,\"sp\"	TEXT,\"state\"	TEXT,FOREIGN KEY(\"projectID\") REFERENCES \"Projects\"(\"projectID\"))");
    sendQuery("CREATE TABLE \"IdentifiedAddressesPorts\" (\"portID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"identifiedAddressID\"	INTEGER,\"portType\"	TEXT,\"portNum\"	TEXT,\"portState\"	TEXT,FOREIGN KEY(\"identifiedAddressID\") REFERENCES \"IdentifiedAddressesPorts\"(\"identifiedAddressID\"))");
    sendQuery("CREATE TABLE \"MissingHeaders\" (\"missingHeaderID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"projectID\"	INTEGER NOT NULL,\"missingHeader\"	TEXT,\"detail\"	TEXT)");
    sendQuery("CREATE TABLE \"OwaspTop10\" (\"vulnerabilityID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"vulnerabilityNo\"	TEXT,\"vulnerability\"	TEXT,\"vulnerabilityInfo\"	TEXT)");
    sendQuery("CREATE TABLE \"ProjectAuthors\" (\"authorID\"	INTEGER NOT NULL,\"projectID\"	INTEGER NOT NULL,FOREIGN KEY(\"projectID\") REFERENCES \"Projects\"(\"projectID\"),FOREIGN KEY(\"authorID\") REFERENCES \"Authors\"(\"authorID\"))");
    sendQuery("CREATE TABLE \"ProjectOwasp\" (\"projectID\"	INTEGER NOT NULL,\"vulnerabilityID\"	INTEGER NOT NULL,FOREIGN KEY(\"vulnerabilityID\") REFERENCES \"OwaspTop10\"(\"vulnerabilityID\"),FOREIGN KEY(\"projectID\") REFERENCES \"Projects\"(\"projectID\"))");
    sendQuery("CREATE TABLE \"Projects\" (\"projectID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"projectName\"	TEXT,\"projectDate\"	TEXT,\"projectInfo\"	TEXT)");
    sendQuery("CREATE TABLE \"TestedUserNames\" (\"testedUserNameID\"	INTEGER PRIMARY KEY AUTOINCREMENT,\"projectID\"	INTEGER,\"userName\"	TEXT,\"userPassword\"	TEXT)");

    for (int i = 0; i < 10; ++i) {
        sendQuery("INSERT INTO OwaspTop10 ('vulnerabilityNo', 'vulnerability', 'vulnerabilityInfo') VALUES ('A" + QString::number(i) + "','Name " + QString::number(i) + "','Info " + QString::number(i) + "')");
    }
}



void db::error(QString err)
{
    qWarning() << "ERROR: " << err;
    QMessageBox messageBox;
    messageBox.setText(err);
    messageBox.setWindowTitle("error");
    messageBox.exec();
}
