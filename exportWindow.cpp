#include "exportWindow.h"
#include "ui_exportWindow.h"

#include <QFileInfo>
#include <qdebug.h>
#include <QDir>

exportWindow::exportWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::exportWindow)
{
    ui->setupUi(this);
    refreshComboBoxes();
}



exportWindow::~exportWindow()
{
    delete ui;
}

void exportWindow::on_pb_print_clicked()
{
    reportName = ui->le_reportName->text();

    if (QFileInfo(reportName).suffix().isEmpty()) { reportName.append(".html"); }

    //Read content of template file
    QFile templateFile(QDir::currentPath() + templatesFolderName + "/" + ui->cb_template->currentText());
    if (!templateFile.open(QFile::ReadOnly | QFile::Text)){
        qWarning() << "Failed to read content of tempalate file: " << ui->cb_template->currentText();
        return;
    }
    QTextStream inTemplate(&templateFile);
    reportTemplate = inTemplate.readAll();

    //Read content of css file
    QFile cssFile(QDir::currentPath() + cssFolderName + "/" + ui->cb_css->currentText());
    if (!cssFile.open(QFile::ReadOnly | QFile::Text)){
        qWarning() << "Failed to read content of CSS file: " << ui->cb_css->currentText();
        return;
    }

    QTextStream inCss(&cssFile);
    reportCSS = inCss.readAll();

    this->accept();
}

void exportWindow::refreshComboBoxes()
{    // Fill in templates
    ui->cb_template->clear();
    QDir* templateDir = new QDir(QDir::currentPath() + templatesFolderName);
    QFileInfoList templateFiles = templateDir->entryInfoList(QDir::Files);
    for(int i = 0; i < templateFiles.length(); i++){
        if(templateFiles.at(i).suffix() == "html"){
            ui->cb_template->addItem(templateFiles.at(i).fileName());
        }
    }

    // Fill in css files
    ui->cb_css->clear();
    QDir* cssDir = new QDir(QDir::currentPath() + cssFolderName);
    QFileInfoList cssFiles = cssDir->entryInfoList(QDir::Files);
    for(int i = 0; i < cssFiles.length(); i++){
        if(cssFiles.at(i).suffix() == "css"){
            ui->cb_css->addItem(cssFiles.at(i).fileName());
        }
    }
}
