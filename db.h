#ifndef DB_H
#define DB_H

#include <QObject>
#include <QSqlDatabase>
#include <QString>
#include <QSqlTableModel>

class db : public QObject
{
    Q_OBJECT

public:
    explicit db(QObject *parent = nullptr);

    QSqlTableModel* getTableModel(QString tableName);
    QSqlTableModel* getProjectsModel();


    QStringList getAllAuthors();
    QStringList getAllVulnerabilities();

    QList<QStringList> getVulnerabilitiesTable();
    QList<QStringList> getIdentifiedTargets(int projectID);
    QString getIdenfiedTargetByID(QString targetID);
    QString getIdenfiedTargetIdByIpAndProjectID(QString ipv4, QString projectID);
    QStringList getIdentifiedTargetsIds(int projectID);
    QList<QStringList> getIdentifiedTargetsPorts(QString identifiedAddressID);
    QList<QStringList> getDetectedVulnerabilities(int projectID);
    QList<QStringList> getProjectVulnerabilities(int projectID);
    QList<QStringList> getTestedUserNames(int projectID);
    QList<QStringList> getCrackedUsers(int projectID);
    QList<QStringList> getMissingHeaders(int projectID);
    QStringList getProjectAuthors(int projectID);
    QStringList getProjectVulnerabilitiesNames(int projectID);

    QString getProjectName(int id);
    QString getProjectInfo(int id);
    QDate getProjectDate(int id);
    QString getCustomString(QString type, int id);

    void setProjectName(int id, QString name);
    void setProjectInfo(int id, QString info);
    void setProjectDate(int id, QDate date);
    void setCustomString(QString type, QString string, int id);

    int addNewProject();
    void addAuthor(QString name);
    void addAuthorToProject(QString authorName, int projectId);
    void addVulnerabilityToProject(QString vulnerabilityName, int projectId);
    void addIdentifiedAddress(QString ipv4, QString dns, QString as, QString sp, QString state, int projectId);
    void addIdentifiedAddressPort(int identifiedAddressID, QString portType, QString portNum, QString portState);
    void addDetectedVulnerability(QStringList params);
    void addTestedUserName(QString username, QString password, int projectId);
    void addMissingHeader(QString missingHeader, QString detail, int projectId);
    void addAttachment(QString attachment, int projectId);

    void removeAuthorFromProject(QString authorName, int projectId);
    void removeVulnerabilityFromProject(QString vulnerabilityName, int projectId);
    void removeIdentifiedAddress(int id);
    void removeIdentifiedAddress(QString id);
    void removeIdentifiedAddressPort(QString id);
    void removeDetectedVulnerability(QString id);
    void removeTestedUserName(QString id);
    void removeMissingHeader(QString id);
    void removeProject(QString id);
    void removeAuthor(QString authorID);
    void removeAttachment(QString id);


private:
    const QString DRIVER = "QSQLITE";
    const QString dbDirectory = "database";
    const QString dbName = "default.db";

    QString getString(QString whatToGet, QString table, QString identifierName, QString indentifierValue);
    QString getStringDoubleFiltered(QString whatToGet, QString table, QString identifierName1, QString indentifierValue1, QString identifierName2, QString indentifierValue2);
    QStringList getStringList(QString whatToGet, QString table);
    QStringList getStringListFiltered(QString whatToGet, QString table, QString identifierName, QString indentifierValue);
    QList<QStringList> getTable(QStringList columnNames, QString tableName);
    QList<QStringList> getTableFiltered(QStringList columnNames, QString tableName, QString identifierName, QString identifierValue);

    void setString(QString table, QString whatToSet, QString value, QString identifierName, QString id);
    void removeById(QString table, QString identifierName, QString identifierValue);
    void inserNew(QStringList columnNames, QString tableName, QStringList parameters);
    void sendQuery(QString query);
    void initializeDb();
    void initializeNewDb();
    void error(QString err);
};

#endif // DB_H
