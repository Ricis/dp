#include "importService.h"


importService::importService(QObject *parent, db* database, int* activeProject) : QObject(parent)
{
    this->database = database;
    this->activeProject = activeProject;
}

void importService::importXML(QString path)
{
    QDomDocument xml;
    QFile f(path);
    if (!f.open(QIODevice::ReadOnly ))
    {
        // Error while loading file
        qWarning() << "Error while loading file: " + path;
        return;
    }

    xml.setContent(&f);
    f.close();

    QDomElement root = xml.documentElement();
    QString type = root.tagName();

    if(type == "nmap")
        importNmap(root);


}

void importService::importNmap(QDomElement root)
{
    QDomNodeList hosts = root.elementsByTagName("host");

    for (int hostI = 0; hostI < hosts.count(); ++hostI) {
        QDomElement host = hosts.at(hostI).toElement();
        QString address = host.attribute("ipAddr");
        database->addIdentifiedAddress(address, "dns", "as", "sp", "state", *activeProject);
        QString addressId = database->getIdenfiedTargetIdByIpAndProjectID(address, QString::number(*activeProject));

        QDomNodeList ports = host.elementsByTagName("port");
        for (int portI = 0; portI < ports.count(); ++portI) {
            QString portType = ports.at(portI).toElement().attribute("protocol");
            QString portNum = ports.at(portI).toElement().attribute("id");
            QString portState = ports.at(portI).toElement().attribute("state");
            database->addIdentifiedAddressPort(addressId.toInt(), portType, portNum, portState);
        }
    }
}
