#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "startupwindow.h"
#include "adddetectedvulnerability.h"
#include "addPortWindow.h"

#include <QDebug>
#include <QCloseEvent>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileInfo>
#include <QFileDialog>
#include <QMimeData>
#include <QClipboard>
#include <QDesktopServices>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    startupWindow* window = new startupWindow(this, database);
    connect(window, &startupWindow::projectSelected, this, &MainWindow::changeActiveProject);
    connect(window, &startupWindow::removeProjectFolder, this, &MainWindow::removeProjectFolder);
    window->exec();
    delete window;
    refreshGUI();
}

MainWindow::~MainWindow()
{
    delete import;
    delete dbEditor;
    delete printer;
    delete authorsEditor;
    delete database;
    delete ui;
}

void MainWindow::addPicture(QString pictureType)
{
    //Get file
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Image"), "/home", tr("Image Files (*.png *.jpg *.bmp *.tif *.gif)"));
    if(fileName.isEmpty())
        return;
    QFileInfo picture = QFileInfo(fileName);

    QString folderPath = QDir::currentPath() + projectsFolder + "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }
    folderPath = QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject)+ "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }
    folderPath = QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject) + imageFolder + "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }

    QString finalPicturePath = folderPath + picture.fileName();

    if (QFile::exists(finalPicturePath))
    {
        QFile::remove(finalPicturePath);
    }
    QFile::copy(fileName, finalPicturePath);
    QFileInfo pictureFinal = QFileInfo(finalPicturePath);

    database->setCustomString(pictureType, pictureFinal.fileName(), activeProject);
}

void MainWindow::addPictureFromClipboard(QString pictureType, QString pictureName)
{
    const QClipboard *clipboard = QApplication::clipboard();
    const QMimeData *mimeData = clipboard->mimeData();

    if (!mimeData->hasImage()) {
        return;
    }
    QString folderPath = QDir::currentPath() + projectsFolder + "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }
    folderPath = QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject)+ "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }
    folderPath = QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject) + imageFolder + "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }
    QString finalPicturePath = folderPath + pictureName + ".jpg";
    if (QFile::exists(finalPicturePath))
    {
        QFile::remove(finalPicturePath);
    }

    QFileInfo pictureFinal = QFileInfo(finalPicturePath);

    if(qvariant_cast<QPixmap>(mimeData->imageData()).save(finalPicturePath, "JPG")){
        database->setCustomString(pictureType, pictureFinal.fileName(), activeProject);
    }

}

QPixmap MainWindow::getPicture(QString pictureType)
{
    QString path = QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject) + imageFolder + "/" + database->getCustomString(pictureType, activeProject);
    QPixmap image = QPixmap(path);
    image = image.scaled(500,500, Qt::AspectRatioMode::KeepAspectRatio, Qt::TransformationMode::FastTransformation);
    return image;
}

void MainWindow::refreshGUI()
{
    // Report info
    ui->le_reportName->setText(database->getProjectName(activeProject));
    ui->de_selectedDate->setDate(database->getProjectDate(activeProject));
    ui->le_reportInfo->setText(database->getProjectInfo(activeProject));

    refreshAuthors();
    refreshOwasp();


    QSqlTableModel* identifiedAddressesModel = database->getTableModel("IdentifiedAddresses");
    identifiedAddressesModel->setFilter("projectID = " + QString::number(activeProject));
    ui->tv_identifiedAddresses->setModel(identifiedAddressesModel);
    ui->tv_identifiedAddresses->hideColumn(0);
    ui->tv_identifiedAddresses->hideColumn(1);

    ui->tv_identifiedAddressesPort->setModel(nullptr);

    ui->te_targetsCustomText->setText(database->getCustomString("targetsCustomText",activeProject));

    //WHOIS
    ui->pte_whoIs->document()->setPlainText(database->getCustomString("whoIs",activeProject));

    //Vulnerabilities
    QSqlTableModel* detectedVulnerabilitiesModel = database->getTableModel("DetectedVulnerabilities");
    detectedVulnerabilitiesModel->setFilter("projectID = " + QString::number(activeProject));
    ui->tv_detectedVulnerabilities->setModel(detectedVulnerabilitiesModel);
    ui->tv_detectedVulnerabilities->hideColumn(0);
    ui->tv_detectedVulnerabilities->hideColumn(1);

    //Set report vulnerability custom text
    ui->te_vulnerabilitiesCustomText->setText(database->getCustomString("vulnerabilitiesCustomText",activeProject));

    ui->le_serviceName->setText(database->getCustomString("passwordAttackServiceName", activeProject));
    ui->le_wordlist->setText(database->getCustomString("wordlist", activeProject));

    //set tested user passwords model
    QSqlTableModel* TestedUserNamesModel = database->getTableModel("TestedUserNames");
    TestedUserNamesModel->setFilter("projectID = " + QString::number(activeProject));
    ui->tv_testedUserNames->setModel(TestedUserNamesModel);
    ui->tv_testedUserNames->hideColumn(0);
    ui->tv_testedUserNames->hideColumn(1);

    ui->te_passwordAttackCustomText->setText( database->getCustomString("passwordAttacksCustomText", activeProject));

    //Load picture BurpSuiteScreenShot
    ui->lb_BurpSuiteScreenShot->setPixmap(getPicture("BurpSuiteScreenShot"));
    //Load custom SQLI text
    ui->te_sqliCustomText->setText(database->getCustomString("sqliCustomText", activeProject));

    //Load picture zapScreenShot
    ui->lb_zapScreenShot->setPixmap(getPicture("zapScreenShot"));
    //Load custom zap text
    ui->te_zapCustomText->setText(database->getCustomString("zapCustomText", activeProject));

    //Load Qualys
    ui->te_qualysCustomText->setText(database->getCustomString("qualysCustomText", activeProject));
    ui->lb_qualysScreenShot->setPixmap(getPicture("qualysScreenShot"));
    ui->te_qualysConfigurationErrors->setText(database->getCustomString("qualysConfigurationErrors", activeProject));
    ui->te_qualysCiphersErrors->setText(database->getCustomString("qualysCiphersErrors", activeProject));

    //Load Sophos
    ui->te_sophosCustomTextBefore->setText(database->getCustomString("sophosCustomTextBefore", activeProject));
    ui->lb_sophosScreenShot->setPixmap(getPicture("sophosScreenShot"));
    ui->te_sophosCustomTextAfter->setText(database->getCustomString("sophosCustomTextAfter", activeProject));

    QSqlTableModel* missingHeadersModel = database->getTableModel("MissingHeaders");
    missingHeadersModel->setFilter("projectID = " + QString::number(activeProject));
    ui->tv_missingHeaders->setModel(missingHeadersModel);
    ui->tv_missingHeaders->hideColumn(0);
    ui->tv_missingHeaders->hideColumn(1);

    //Load AV
    ui->te_avCustomTextBefore->setText(database->getCustomString("avCustomTextBefore", activeProject));
    ui->lb_avScreenShot->setPixmap(getPicture("avScreenShot"));

    //Load summary
    ui->pte_summary->setPlainText(database->getCustomString("summary", activeProject));

    //Load attachments
    QSqlTableModel* attachmentsModel = database->getTableModel("Attachments");
    attachmentsModel->setFilter("projectID = " + QString::number(activeProject));
    ui->tv_attachments->setModel(attachmentsModel);
    ui->tv_attachments->hideColumn(0);
    ui->tv_attachments->hideColumn(1);

}

void MainWindow::refreshAuthors()
{
    // Fill all authors
    QStringList allAuthors = database->getAllAuthors();
    ui->lw_authorsAll->clear();
    for(int i = 0; i < allAuthors.length(); i++){
        ui->lw_authorsAll->addItem(allAuthors.at(i));
    }
    // Fill selected authors
    ui->lw_authorsSelected->clear();
    QStringList selectedAuthors = database->getProjectAuthors(activeProject);
    for(int i = 0; i < selectedAuthors.length(); i++){
        ui->lw_authorsSelected->addItem(selectedAuthors.at(i));
    }

}

void MainWindow::refreshOwasp()
{
    // Fill all owasp
    QStringList allVulnerabilities = database->getAllVulnerabilities();
    ui->lw_OWASPAll->clear();
    for(int i = 0; i < allVulnerabilities.length(); i++){
        ui->lw_OWASPAll->addItem(allVulnerabilities.at(i));
    }
    // Fill selected owasp
    ui->lw_OWASPSelected->clear();
    QStringList selectedVulnerabilities = database->getProjectVulnerabilitiesNames(activeProject);
    for(int i = 0; i < selectedVulnerabilities.length(); i++){
        ui->lw_OWASPSelected->addItem(selectedVulnerabilities.at(i));
    }

}

void MainWindow::refreshIdentifiedAddressPort(int identifiedAddressID)
{
    QSqlTableModel* identifiedAddressPorts = database->getTableModel("IdentifiedAddressesPorts");
    identifiedAddressPorts->setFilter("identifiedAddressID = " + QString::number(identifiedAddressID));
    ui->tv_identifiedAddressesPort->setModel(identifiedAddressPorts);
    ui->tv_identifiedAddressesPort->hideColumn(0);
    ui->tv_identifiedAddressesPort->hideColumn(1);
}

QString MainWindow::escapeString(QString in)
{
    return in.replace("'", "''");
}

void MainWindow::importXml()
{
    //Get file
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open xml"), "/home", tr("xml (*.xml)"));
    if(fileName.isEmpty())
        return;

    import->importXML(fileName);
}

void MainWindow::openActiveProjectFolder()
{
    QDesktopServices::openUrl(QUrl(QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject), QUrl::TolerantMode));
}


// SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS SLOTS

void MainWindow::changeActiveProject(int project)
{
    qWarning() << "Changing project " << QString::number(project);
    activeProject = project;

    QString folderPath = QDir::currentPath() + projectsFolder + "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }
    folderPath = QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject)+ "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }

    refreshGUI();
}

void MainWindow::removeProjectFolder(QString projectID)
{
    qWarning() << "Removing project folder " << projectID;
    QDir projectFolder(QDir::currentPath() + projectsFolder + "/" + projectID);
    projectFolder.removeRecursively();
}

void MainWindow::on_action_editOwaspTop10_triggered()
{
    dbEditor->exec();
    refreshGUI();
}

void MainWindow::on_actionEdit_authors_triggered()
{
    authorsEditor->exec();
    refreshGUI();
}

void MainWindow::on_actionOpen_project_folder_triggered()
{
    openActiveProjectFolder();
}

void MainWindow::on_actionChange_project_triggered()
{
    startupWindow* window = new startupWindow(this, database);
    connect(window, &startupWindow::projectSelected, this, &MainWindow::changeActiveProject);
    connect(window, &startupWindow::removeProjectFolder, this, &MainWindow::removeProjectFolder);
    window->exec();
    delete window;
}

void MainWindow::on_de_selectedDate_dateChanged(const QDate &date)
{
    database->setProjectDate(activeProject, date);
}

void MainWindow::on_le_reportName_textEdited(const QString &arg1)
{
    database->setProjectName(activeProject, escapeString(arg1));
}

void MainWindow::on_le_reportInfo_textEdited(const QString &arg1)
{
    database->setProjectInfo(activeProject, escapeString(arg1));
}

void MainWindow::on_lw_authorsAll_itemDoubleClicked(QListWidgetItem *item)
{
    // Check if is not already assigned to this project
    for (int i=0; i < ui->lw_authorsSelected->count(); i++) {
        if(ui->lw_authorsSelected->item(i)->text() == item->text())
            return;
    }

    database->addAuthorToProject(item->text(), activeProject);
    refreshAuthors();

}

void MainWindow::on_lw_authorsSelected_itemDoubleClicked(QListWidgetItem *item)
{
    database->removeAuthorFromProject(item->text(), activeProject);
    refreshAuthors();
}

void MainWindow::on_lw_OWASPAll_itemDoubleClicked(QListWidgetItem *item)
{
    // Check if is not already assigned to this project
    for (int i=0; i < ui->lw_OWASPSelected->count(); i++) {
        if(ui->lw_OWASPSelected->item(i)->text() == item->text())
            return;
    }

    database->addVulnerabilityToProject(item->text(), activeProject);
    refreshOwasp();
}

void MainWindow::on_lw_OWASPSelected_itemDoubleClicked(QListWidgetItem *item)
{
    database->removeVulnerabilityFromProject(item->text(), activeProject);
    refreshOwasp();
}

void MainWindow::on_pb_addIdentifiedAddress_clicked()
{
    database->addIdentifiedAddress("ipv4", "dns", "as", "sp", "state", activeProject);
    refreshGUI();
}

void MainWindow::on_pb_removeIdentifiedAddress_clicked()
{
    QItemSelectionModel *select = ui->tv_identifiedAddresses->selectionModel();
    if(!select->hasSelection()){
        qWarning() << "no selection";
        return;
    }

    QModelIndexList selectedItems = select->selectedRows();
    for (int i = 0; i < selectedItems.length(); i++) {

        QString id = selectedItems.at(i).data().toString();
        qWarning() << id;

        if(!id.isEmpty()){
            database->removeIdentifiedAddress(id);
        }
    }

    delete select;
    refreshGUI();
}

void MainWindow::on_pb_addIdentifiedAddressPort_clicked()
{
    if(!ui->tv_identifiedAddresses->selectionModel()->hasSelection())
        return;

    int index = ui->tv_identifiedAddresses->selectionModel()->selectedRows().first().row();
    int identifiedAddressID = ui->tv_identifiedAddresses->model()->index(index, 0).data().toInt();


    addPortWindow* window = new addPortWindow();
    int res = window->exec();
    if(res == 1){
        database->addIdentifiedAddressPort(identifiedAddressID, window->portType, window->portNum, window->portState);
        refreshIdentifiedAddressPort(identifiedAddressID);
    }

    delete window;
}

void MainWindow::on_te_targetsCustomText_textChanged()
{
    database->setCustomString("targetsCustomText", escapeString(ui->te_targetsCustomText->toPlainText()), activeProject);
}

void MainWindow::on_pb_removeIdentifiedAddressPort_clicked()
{
    QItemSelectionModel *select = ui->tv_identifiedAddressesPort->selectionModel();
    if(!select->hasSelection()){
        return;
    }

    int identifiedAddressID = ui->tv_identifiedAddressesPort->model()->index(0,1).data().toInt();

    QModelIndexList selectedItems = select->selectedRows();
    for (int i = 0; i < selectedItems.length(); i++) {
        QString id = selectedItems.at(i).data().toString();

        qWarning() << id;

        if(!id.isEmpty()){
            database->removeIdentifiedAddressPort(id);
        }
    }
    qWarning() << "identifiedAddressID: " + QString::number(identifiedAddressID);
    refreshIdentifiedAddressPort(identifiedAddressID);

    delete select;
}

void MainWindow::on_tv_identifiedAddresses_clicked(const QModelIndex &index)
{
    int identifiedAddressID = ui->tv_identifiedAddresses->model()->index(index.row(), 0).data().toInt();
    refreshIdentifiedAddressPort(identifiedAddressID);
}

void MainWindow::on_pte_whoIs_textChanged()
{
    database->setCustomString("whoIs", escapeString(ui->pte_whoIs->toPlainText()), activeProject);
}

void MainWindow::on_pb_addDetectedVulnerability_clicked()
{
    addDetectedVulnerability* window = new addDetectedVulnerability();
    int res = window->exec();
    if(res == 1){
        QStringList values;
        values << QString::number(activeProject);
        values << window->ip;
        values << window->port;
        values << window->service;
        values << window->vulnerability;
        values << window->risk;
        values << window->possibleImpact;
        values << window->recommendation;
        database->addDetectedVulnerability(values);
        refreshGUI();
    }

    delete window;
}

void MainWindow::on_pb_removeDetectedVulnerability_clicked()
{
    QItemSelectionModel *select = ui->tv_detectedVulnerabilities->selectionModel();
    if(!select->hasSelection()){
        qWarning() << "no selection";
        return;
    }

    QModelIndexList selectedItems = select->selectedRows();
    for (int i = 0; i < selectedItems.length(); i++) {

        QString id = selectedItems.at(i).data().toString();
        qWarning() << id;

        if(!id.isEmpty()){
            database->removeDetectedVulnerability(id);
        }
    }
    refreshGUI();

    delete select;
}

void MainWindow::on_te_vulnerabilitiesCustomText_textChanged()
{
    database->setCustomString("vulnerabilitiesCustomText", escapeString(ui->te_vulnerabilitiesCustomText->toPlainText()), activeProject);
}

void MainWindow::on_pb_testedUsernNamesAdd_clicked()
{
    database->addTestedUserName("", "", activeProject);
    refreshGUI();
}

void MainWindow::on_pb_testedUsernNamesRemove_clicked()
{
    QItemSelectionModel *select = ui->tv_testedUserNames->selectionModel();
    if(!select->hasSelection()){
        qWarning() << "no selection";
        return;
    }

    QModelIndexList selectedItems = select->selectedRows();
    for (int i = 0; i < selectedItems.length(); i++) {

        QString id = selectedItems.at(i).data().toString();

        if(!id.isEmpty()){
            database->removeTestedUserName(id);
        }
    }
    refreshGUI();

    delete select;
}

void MainWindow::on_le_serviceName_textChanged(const QString &arg1)
{
    database->setCustomString("passwordAttackServiceName", escapeString(arg1), activeProject);
}

void MainWindow::on_le_wordlist_textChanged(const QString &arg1)
{
    database->setCustomString("wordlist", escapeString(arg1), activeProject);
}

void MainWindow::on_te_passwordAttackCustomText_textChanged()
{
    database->setCustomString("passwordAttacksCustomText", escapeString(ui->te_passwordAttackCustomText->toPlainText()), activeProject);
}

void MainWindow::on_pb_burpSuitePictureBrowse_clicked()
{
    addPicture("BurpSuiteScreenShot");
    ui->lb_BurpSuiteScreenShot->setPixmap(getPicture("BurpSuiteScreenShot"));
}

void MainWindow::on_te_sqliCustomText_textChanged()
{
    database->setCustomString("sqliCustomText", escapeString(ui->te_sqliCustomText->toPlainText()), activeProject);
}

void MainWindow::on_pb_zapScreenshotBrowse_clicked()
{
    addPicture("zapScreenShot");
    ui->lb_zapScreenShot->setPixmap(getPicture("zapScreenShot"));
}

void MainWindow::on_te_zapCustomText_textChanged()
{
    database->setCustomString("zapCustomText", escapeString(ui->te_zapCustomText->toPlainText()), activeProject);
}

void MainWindow::on_pb_burpSuitePictureClipBoard_clicked()
{
    addPictureFromClipboard("BurpSuiteScreenShot", "BurpSuiteScreenShot");
    ui->lb_BurpSuiteScreenShot->setPixmap(getPicture("BurpSuiteScreenShot"));
}

void MainWindow::on_pb_zapPictureClipBoard_clicked()
{
    addPictureFromClipboard("zapScreenShot", "zapScreenShot");
    ui->lb_zapScreenShot->setPixmap(getPicture("zapScreenShot"));
}

void MainWindow::on_te_qualysCustomText_textChanged()
{
    database->setCustomString("qualysCustomText", escapeString(ui->te_qualysCustomText->toPlainText()), activeProject);
}

void MainWindow::on_pb_qualysScreenshotBrowse_clicked()
{
    addPicture("qualysScreenShot");
    ui->lb_qualysScreenShot->setPixmap(getPicture("qualysScreenShot"));
}

void MainWindow::on_pb_qualysPictureClipBoard_clicked()
{
    addPictureFromClipboard("qualysScreenShot", "qualysScreenShot");
    ui->lb_qualysScreenShot->setPixmap(getPicture("qualysScreenShot"));
}

void MainWindow::on_te_qualysConfigurationErrors_textChanged()
{
    database->setCustomString("qualysConfigurationErrors", escapeString(ui->te_qualysConfigurationErrors->toPlainText()), activeProject);
}

void MainWindow::on_te_qualysCiphersErrors_textChanged()
{
    database->setCustomString("qualysCiphersErrors", escapeString(ui->te_qualysCiphersErrors->toPlainText()), activeProject);
}

void MainWindow::on_te_sophosCustomTextBefore_textChanged()
{
    database->setCustomString("sophosCustomTextBefore", escapeString(ui->te_sophosCustomTextBefore->toPlainText()), activeProject);
}

void MainWindow::on_pb_sophosScreenShotBrowse_clicked()
{
    addPicture("sophosScreenShot");
    ui->lb_sophosScreenShot->setPixmap(getPicture("sophosScreenShot"));
}

void MainWindow::on_pb_sophosScreenShotClipBoard_clicked()
{
    addPictureFromClipboard("sophosScreenShot", "sophosScreenShot");
    ui->lb_sophosScreenShot->setPixmap(getPicture("sophosScreenShot"));
}

void MainWindow::on_te_sophosCustomTextAfter_textChanged()
{
    database->setCustomString("sophosCustomTextAfter", escapeString(ui->te_sophosCustomTextAfter->toPlainText()), activeProject);
}

void MainWindow::on_pb_missingHeadersAdd_clicked()
{
    database->addMissingHeader("", "", activeProject);
    refreshGUI();
}

void MainWindow::on_pb_missingHeadersRemove_clicked()
{
    QItemSelectionModel *select = ui->tv_missingHeaders->selectionModel();
    if(!select->hasSelection()){
        qWarning() << "no selection";
        return;
    }

    QModelIndexList selectedItems = select->selectedRows();
    for (int i = 0; i < selectedItems.length(); i++) {

        QString id = selectedItems.at(i).data().toString();

        if(!id.isEmpty()){
            database->removeMissingHeader(id);
        }
    }
    refreshGUI();

    delete select;
}

void MainWindow::on_te_avCustomTextBefore_textChanged()
{
    database->setCustomString("avCustomTextBefore", escapeString(ui->te_avCustomTextBefore->toPlainText()), activeProject);
}

void MainWindow::on_pb_avScreenShotBrowse_clicked()
{
    addPicture("avScreenShot");
    ui->lb_avScreenShot->setPixmap(getPicture("avScreenShot"));
}

void MainWindow::on_pb_avScreenShotClipBoard_clicked()
{
    addPictureFromClipboard("avScreenShot", "avScreenShot");
    ui->lb_avScreenShot->setPixmap(getPicture("avScreenShot"));
}

void MainWindow::on_pb_summaryBold_clicked()
{
    QTextCursor cursor = ui->pte_summary->textCursor();
    QString text = cursor.selectedText();
    text.prepend("<b>");
    text.append("</b>");
    cursor.removeSelectedText();
    cursor.insertText(text);
}

void MainWindow::on_pb_summaryItalic_clicked()
{
    QTextCursor cursor = ui->pte_summary->textCursor();
    QString text = cursor.selectedText();
    text.prepend("<i>");
    text.append("</i>");
    cursor.removeSelectedText();
    cursor.insertText(text);
}

void MainWindow::on_pb_summaryUnsortedList_clicked()
{
    QTextCursor cursor = ui->pte_summary->textCursor();
    cursor.insertText("<ul><li>text 1</li> \n <li>text 2</li> \n <li>text 3</li></ul>");
}

void MainWindow::on_pb_summaryUnderline_clicked()
{
    QTextCursor cursor = ui->pte_summary->textCursor();
    QString text = cursor.selectedText();
    text.prepend("<u>");
    text.append("</u>");
    cursor.removeSelectedText();
    cursor.insertText(text);
}

void MainWindow::on_pte_summary_textChanged()
{
    database->setCustomString("summary", escapeString(ui->pte_summary->toPlainText()), activeProject);
}

void MainWindow::on_pb_addAtachment_clicked()
{
    //Get file
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open attachment"), "/home", tr("PDF (*.pdf)"));
    if(fileName.isEmpty())
        return;
    QFileInfo attachment = QFileInfo(fileName);

    QString folderPath = QDir::currentPath() + projectsFolder + "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }
    folderPath = QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject)+ "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }
    folderPath = QDir::currentPath() + projectsFolder + "/" + QString::number(activeProject) + attachmentsFolder + "/" ;
    if(!QDir(folderPath).exists()){
        QDir().mkdir(folderPath);
    }

    QString finalAttachmentPath = folderPath + attachment.fileName();

    if (QFile::exists(finalAttachmentPath))
    {
        QFile::remove(finalAttachmentPath);
    }
    QFile::copy(fileName, finalAttachmentPath);

    database->addAttachment(attachment.fileName(), activeProject);
    refreshGUI();
}

void MainWindow::on_pb_removeAttachment_clicked()
{
    QItemSelectionModel *select = ui->tv_attachments->selectionModel();
    if(!select->hasSelection()){
        qWarning() << "no selection";
        return;
    }

    QModelIndexList selectedItems = select->selectedRows();
    for (int i = 0; i < selectedItems.length(); i++) {

        QString id = selectedItems.at(i).data().toString();

        if(!id.isEmpty()){
            database->removeAttachment(id);
        }
    }
    refreshGUI();

    delete select;
}

void MainWindow::on_pb_browseIdentifiedAddress_clicked()
{
    importXml();
    refreshGUI();
}

void MainWindow::on_actionExport_project_triggered()
{
    QString html;
    QString css;
    QString fileName;

    exportWindow* window = new exportWindow();
    int res = window->exec();
    if(res != 1){
        return;
    }

    html = window->reportTemplate;
    css = window->reportCSS;
    fileName = window->reportName;
    printer->print(fileName, html, css);


    QMessageBox msgBox;
    msgBox.setText("Don't forget to use your attachments");
    msgBox.exec();

    delete window;
    openActiveProjectFolder();
}



