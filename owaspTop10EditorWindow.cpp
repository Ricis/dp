#include "owaspTop10EditorWindow.h"
#include "ui_dbEditorService.h"

#include <QDebug>
#include <QMessageBox>
#include <QInputDialog>

owaspTop10EditorWindow::owaspTop10EditorWindow(QWidget *parent, db *database) :
    QDialog(parent),
    ui(new Ui::owaspTop10EditorWindow)
{
    ui->setupUi(this);
    this->database = database;
    QSqlTableModel* owaspTop10 = database->getTableModel("OwaspTop10");
    ui->tv_db->setModel(owaspTop10);
    ui->tv_db->hideColumn(0);
}

owaspTop10EditorWindow::~owaspTop10EditorWindow()
{
    delete ui;
}
